package com.tcwgq.mapper;

import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年9月13日下午9:58:05
 * @email tcwgq@outlook.com
 */
public interface UserMapper {

	public User getById(Integer id) throws Exception;

}
