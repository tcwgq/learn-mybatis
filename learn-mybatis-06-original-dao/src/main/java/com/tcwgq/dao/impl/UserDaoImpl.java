package com.tcwgq.dao.impl;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import com.tcwgq.dao.UserDao;
import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年9月13日下午10:00:09
 * @email tcwgq@outlook.com
 */
public class UserDaoImpl implements UserDao {

	private SqlSessionFactory sqlSessionFactory;

	public UserDaoImpl(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	@Override
	public User getById(Integer id) throws Exception {
		SqlSession session = sqlSessionFactory.openSession();
		User user = session.selectOne("user.getById", id);
		session.close();
		return user;
	}

	@Override
	public int insertUser(User user) throws Exception {
		SqlSession session = sqlSessionFactory.openSession();
		int rows = session.insert("user.insetUser", user);
		session.commit();
		session.close();
		return rows;
	}

	@Override
	public int deleteById(Integer id) throws Exception {
		SqlSession session = sqlSessionFactory.openSession();
		int rows = session.delete("user.deleteById", id);
		session.commit();
		session.close();
		return rows;
	}

}
