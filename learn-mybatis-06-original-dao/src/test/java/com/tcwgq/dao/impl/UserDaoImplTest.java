package com.tcwgq.dao.impl;

import static org.junit.Assert.fail;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.tcwgq.dao.UserDao;
import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年9月13日下午10:12:21
 * @email tcwgq@outlook.com
 */
public class UserDaoImplTest {

	private SqlSessionFactory sqlSessionFactory;

	private UserDao dao;

	@Before
	public void setUp() throws Exception {
		// 不能加classpath前缀
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		// Configuration config = new Configuration();
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
		dao = new UserDaoImpl(sqlSessionFactory);
	}

	@Test
	public void testUserDaoImpl() throws Exception {
		User user = dao.getById(10);
		System.out.println(user);
	}

	@Test
	public void testGetById() {
		fail("Not yet implemented");
	}

	@Test
	public void testInsertUser() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

}
