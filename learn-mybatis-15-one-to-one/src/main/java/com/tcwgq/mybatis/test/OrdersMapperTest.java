package com.tcwgq.mybatis.test;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.tcwgq.mapper.OrdersMapper;
import com.tcwgq.model.Orders;
import com.tcwgq.model.OrdersCustom;

/**
 * @author tcwgq
 * @time 2017年9月17日上午11:41:03
 * @email tcwgq@outlook.com
 */
public class OrdersMapperTest {

	private SqlSessionFactory sqlSessionFactory;

	private OrdersMapper mapper;

	@Before
	public void setUp() throws Exception {
		// 不能加classpath前缀
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		// Configuration config = new Configuration();
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

		SqlSession session = sqlSessionFactory.openSession();

		mapper = session.getMapper(OrdersMapper.class);
	}

	@Test
	public void testfindOrdersRusultType() throws Exception {
		List<OrdersCustom> list = mapper.findOrdersRusultType();
		System.out.println(list);
	}

	@Test
	public void findOrdersRusultMap() throws Exception {
		List<Orders> list = mapper.findOrdersRusultMap();
		System.out.println(list);
	}

}
