package com.tcwgq.datasource;

/**
 * 使用ThreadLocal技术来记录当前线程中的数据源的key
 * @author qifei3
 */
public class DynamicDataSourceHolder {

    /**
     * 使用ThreadLocal记录当前线程的数据源key
     */
    private static final ThreadLocal<String> HOLDER = new ThreadLocal<>();

    /**
     * 设置数据源key
     * @param key 数据源标识
     */
    public static void putDataSourceKey(String key) {
        HOLDER.set(key);
    }

    /**
     * 获取数据源key
     * @return 数据源标识
     */
    public static String getDataSourceKey() {
        return HOLDER.get();
    }
}
