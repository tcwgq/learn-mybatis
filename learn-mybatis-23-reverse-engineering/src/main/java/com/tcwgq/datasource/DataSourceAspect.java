package com.tcwgq.datasource;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @author qifei3
 */
@Component
@Aspect
public class DataSourceAspect {

    private final static Logger LOG = LoggerFactory.getLogger(DataSourceAspect.class);

    @Pointcut("execution(* com.jd.galaxy.dao.*.*(..))")
    public void read(){}

    @Before("read()")
    public void BeforeRead(JoinPoint point){
        Object object = point.getTarget();
        //获取方法名称
        String methodName = point.getSignature().getName();
        // 类名
        Class<?>[] clazz = object.getClass().getInterfaces();
        // 参数
        Class<?>[] parameterTypes = ((MethodSignature)point.getSignature()).getMethod().getParameterTypes();
        try {
            //获取到方法
            Method method = clazz[0].getMethod(methodName,parameterTypes);
            if (method != null && method.isAnnotationPresent(DataSource.class)) {
                DataSource dataSource = method.getAnnotation(DataSource.class);
                //获取主从数据库的key以便切换数据库
                DataSourceEnum dbKey = dataSource.value();
                LOG.info("类名:{},方法名:{},使用的数据源:{}",clazz[0].getName(),methodName,dbKey);
                // 设置数据源
                DynamicDataSourceHolder.putDataSourceKey(dbKey.toString().toLowerCase());
            }else{
                LOG.warn("类名:{},方法名:{},没有设置数据源!",clazz[0].getName(),methodName);
                // 使用默认主库
                DynamicDataSourceHolder.putDataSourceKey(DataSourceEnum.MASTER.toString().toLowerCase());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
