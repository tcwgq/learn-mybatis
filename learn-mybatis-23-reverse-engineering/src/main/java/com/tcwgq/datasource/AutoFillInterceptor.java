package com.tcwgq.datasource;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.tcwgq.utils.DateUtils;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by wangguangqiang on 2018/11/10 21:43.
 */
@Slf4j
@Intercepts({@Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})})
public class AutoFillInterceptor implements Interceptor {

    private static final LoadingCache<MethodCacheKey, MethodHandle> METHOD_CACHE = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterWrite(30, TimeUnit.MINUTES)
            .build(new CacheLoader<MethodCacheKey, MethodHandle>() {
                @Override
                public MethodHandle load(MethodCacheKey key) throws Exception {
                    return MethodHandles.lookup().findVirtual(key.getClazz(), key.getMethodName(), key.getMethodType());
                }
            });

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        // 获取 SQL 命令
        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
        // 获取参数
        Object parameter = invocation.getArgs()[1];
        if (parameter != null && SqlCommandType.INSERT == sqlCommandType) {
            for (PropertyDescriptor property : getBean(parameter.getClass()).getPropertyDescriptors()) {
                CommonField field = CommonField.of(property.getName());
                if (field != null && readValue(parameter, property, field) == null) {
                    writeValue(parameter, property, field);
                }
            }
        }
        if (parameter != null && SqlCommandType.UPDATE == sqlCommandType) {
            for (PropertyDescriptor property : getBean(parameter.getClass()).getPropertyDescriptors()) {
                CommonField field = CommonField.of(property.getName());
                if (field == CommonField.LAST_MODIFIED_TIME) {
                    writeValue(parameter, property, field);
                } else if (field == CommonField.LAST_MODIFIED_BY) {
                    if (readValue(parameter, property, field) == null) {
                        writeValue(parameter, property, field);
                    }
                }
            }
        }

        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }

    private BeanInfo getBean(Class<?> clazz) {
        try {
            return Introspector.getBeanInfo(clazz);
        } catch (IntrospectionException e) {
            log.error("Get Bean info: {} failed!", clazz.getName());
            throw new RuntimeException("No such bean info");
        }
    }

    private <T> Object readValue(T instance, PropertyDescriptor property, CommonField field) {
        Class<?> clazz = instance.getClass();
        MethodType methodType = MethodType.methodType(property.getPropertyType());
        Method readMethod = property.getReadMethod();
        return doInvoke(clazz, readMethod.getName(), methodType, instance, null);
    }

    private <T> Object writeValue(T instance, PropertyDescriptor property, CommonField field) {
        Class<?> clazz = instance.getClass();
        MethodType methodType = MethodType.methodType(void.class, property.getPropertyType());
        Method writeMethod = property.getWriteMethod();

        // defaultValue() 返回的值类型，可能和字段类型不一致，
        // 导致无法invoke对应的方法签名, 需要强制类型转换
        Object writeValue = field.defaultValue();
        if (!field.defaultValue().getClass().equals(property.getPropertyType())) {
            writeValue = ConvertUtils.convert(writeValue, property.getPropertyType());
        }
        return doInvoke(clazz, writeMethod.getName(), methodType, instance, writeValue);
    }

    private Object doInvoke(Class<?> clazz, String methodName, MethodType methodType,
                            Object o, Object params) {
        try {
            MethodCacheKey key = new MethodCacheKey().setClazz(clazz).setMethodName(methodName).setMethodType(methodType);
            MethodHandle method = METHOD_CACHE.get(key);
            if (isGetter(methodName)) {
                return method.invokeWithArguments(o);
            }
            return method.invokeWithArguments(o, params);
        } catch (NoSuchMethodException | IllegalAccessException e) {
            // can not be happened in runtime lessly
            log.error("no such method: {}. class: {}", methodName, clazz.getName(), e);
            throw new RuntimeException("no such method");
        } catch (Throwable e) {
            // can not be happened in runtime lessly
            log.error("invoke method: {} failed! Class: {}", methodName, clazz.getName(), e);
            throw new RuntimeException("invoke method failed");
        }
    }

    private static boolean isGetter(String methodName) {
        return methodName.startsWith("get");
    }

    @Accessors(chain = true)
    @Data
    private static class MethodCacheKey {
        private Class clazz;
        private String methodName;
        private MethodType methodType;
    }

    private enum CommonField {

        /**
         * 公共字段
         */
        CREATED_TIME("createdTime") {
            @Override
            protected MethodType readMethod() {
                return DATE_READ_METHOD;
            }

            @Override
            protected MethodType writeMethod() {
                return DATE_WRITE_METHOD;
            }

            @Override
            protected Object defaultValue() {
                return DateUtils.now();
            }
        },
        CREATED_BY("createdBy") {
            @Override
            protected MethodType readMethod() {
                return STRING_READ_METHOD;
            }

            @Override
            protected MethodType writeMethod() {
                return STRING_WRITE_METHOD;
            }

            @Override
            protected Object defaultValue() {
                return "SYSTEM";
            }
        },
        LAST_MODIFIED_TIME("lastModifiedTime") {
            @Override
            protected MethodType readMethod() {
                return DATE_READ_METHOD;
            }

            @Override
            protected MethodType writeMethod() {
                return DATE_WRITE_METHOD;
            }

            @Override
            protected Object defaultValue() {
                return DateUtils.now();
            }
        },
        LAST_MODIFIED_BY("lastModifiedBy") {
            @Override
            protected MethodType readMethod() {
                return STRING_READ_METHOD;
            }

            @Override
            protected MethodType writeMethod() {
                return STRING_WRITE_METHOD;
            }

            @Override
            protected Object defaultValue() {
                return "SYSTEM";
            }
        },
        INDICATOR("indicator") {
            @Override
            protected MethodType readMethod() {
                return INTEGER_READ_METHOD;
            }

            @Override
            protected MethodType writeMethod() {
                return INTEGER_WRITE_METHOD;
            }

            @Override
            protected Object defaultValue() {
                return 1;
            }
        },
        VERSION("version") {
            @Override
            protected MethodType readMethod() {
                return INTEGER_READ_METHOD;
            }

            @Override
            protected MethodType writeMethod() {
                return INTEGER_WRITE_METHOD;
            }

            @Override
            protected Object defaultValue() {
                return 1;
            }
        };

        private static final MethodType DATE_READ_METHOD = MethodType.methodType(Date.class);
        private static final MethodType DATE_WRITE_METHOD = MethodType.methodType(void.class, Date.class);

        private static final MethodType INTEGER_READ_METHOD = MethodType.methodType(Integer.class);
        private static final MethodType INTEGER_WRITE_METHOD = MethodType.methodType(void.class, Integer.class);

        private static final MethodType STRING_READ_METHOD = MethodType.methodType(String.class);
        private static final MethodType STRING_WRITE_METHOD = MethodType.methodType(void.class, String.class);

        @Getter
        private String name;

        CommonField(String name) {
            this.name = name;
        }

        protected abstract MethodType readMethod();

        protected abstract MethodType writeMethod();

        protected abstract Object defaultValue();

        public static CommonField of(String name) {
            for (CommonField field : CommonField.values()) {
                if (field.getName().equals(name)) {
                    return field;
                }
            }
            return null;
        }
    }
}
