package com.tcwgq.datasource;

/**
 * @author qifei3
 */
public enum DataSourceEnum {

    /**
     * 主库，做读写操作
     */
    MASTER,

    /**
     * 从库，做读操作
     */
    SLAVE
}
