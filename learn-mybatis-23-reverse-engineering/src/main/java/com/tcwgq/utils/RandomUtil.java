package com.tcwgq.utils;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by wangguangqiang on 2018/11/21 13:46.
 */
public class RandomUtil {
    private static char[] LETTER = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    /**
     * <p>
     * 返回指定位数的随机数
     * </p>
     *
     * @param count int 指定多少位
     * @return String
     */
    public static String randomNumber(int count) {
        return RandomStringUtils.randomNumeric(count);
    }

    /**
     * <p>
     * 返回指定位数的随机字母
     * </p>
     *
     * @param count int 指定多少位
     * @return String
     */
    public static String randomLetter(int count) {
        return RandomStringUtils.random(count, LETTER);
    }

    /**
     * <p>
     * 将给的字符串按字符拆分，返回指定位数的随机字符
     * </p>
     *
     * @param count int 指定多少位
     * @param chars String 给定字符串
     * @return String
     */
    public static String randomString(int count, String chars) {
        return RandomStringUtils.random(count, chars);
    }

    private RandomUtil() {
    }

}
