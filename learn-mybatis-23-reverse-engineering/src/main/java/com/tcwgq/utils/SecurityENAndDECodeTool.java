package com.tcwgq.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wangguangqiang on 2018/11/21 13:38.
 */
public class SecurityENAndDECodeTool {
    // log
    private static final Log log = LogFactory.getLog(SecurityENAndDECodeTool.class);
    // 原始密钥
    private String resourceKey;
    // 当前密钥版本号
    private String currendKeyVersion;
    // 当前版本密钥列表
    private List<String> currentKeylist;
    // 历史密钥,key->密钥版本号,value->密钥列表
    private Map<String, List<String>> historyKey;

    /**
     * <pre>
     * 加码 - 基于当前版本密钥
     *
     * </pre>
     *
     * @param str String 明文
     * @return String
     */
    public String encrypt(String str) {
        String code = "";
        if (StringUtils.isEmpty(str)) {
            return code;
        }
        // keyIndexStr
        String keyIndexStr = RandomUtil.randomLetter(2);
        // keyIndex
        int keyIndex = getKeyIndex(currendKeyVersion, keyIndexStr);
        if (0 <= keyIndex) {
            try {
                code = keyIndexStr + currendKeyVersion + DESCoder.encrypt(str, DESCoder.initKey(currentKeylist.get(keyIndex)));
            } catch (Exception ex) {
                log.error("encrpty enstr(" + str + ") failure", ex);
            }
        }
        return code;
    }

    /**
     * <pre>
     * 解码 - 基于当前版本密钥
     *
     * 描述
     *   默认返回空串
     *
     * </pre>
     *
     * @param enstr String 密文
     * @return String
     */
    public String decrypt(String enstr) {
        String code = "";
        if (StringUtils.isEmpty(enstr)) {
            return code;
        }
        // keyVer
        String keyVer = enstr.substring(2, 3);
        if (isCurrentVersion(keyVer)) {
            // keyIndex
            int keyIndex = getKeyIndex(keyVer, enstr.substring(0, 2));
            if (0 <= keyIndex) {
                try {
                    code = DESCoder.decrypt(enstr.substring(3), DESCoder.initKey(currentKeylist.get(keyIndex)));
                } catch (Exception ex) {
                    log.error("decrpty enstr(" + enstr + ") failure", ex);
                }
            }
        } else {
            code = decrypt(keyVer, enstr);
        }

        return code;
    }

    /**
     * <pre>
     * 加码 - 基于历史版本密钥
     *
     * </pre>
     *
     * @param keyVer String 密钥版本号
     * @param str    String 明文
     * @return String
     */
    public String encrypt(String keyVer, String str) {
        String code = "";
        if (StringUtils.isEmpty(str)) {
            return code;
        }
        // keyIndexStr
        String keyIndexStr = RandomUtil.randomLetter(2);
        // keyIndex
        int keyIndex = getKeyIndex(keyVer, keyIndexStr);
        if (0 <= keyIndex) {
            try {
                code = keyIndexStr + keyVer + DESCoder.encrypt(str, DESCoder.initKey(historyKey.get(keyVer).get(keyIndex)));
            } catch (Exception ex) {
                log.error("encrpty enstr(" + str + ") failure", ex);
            }
        }
        return code;
    }

    /**
     * <pre>
     * 解码 - 基于历史版本密钥
     *
     * </pre>
     *
     * @param keyVer String 密钥版本号
     * @param enstr  String 密文
     * @return String
     */
    public String decrypt(String keyVer, String enstr) {
        String code = "";
        if (StringUtils.isEmpty(enstr)) {
            return code;
        }
        // keyIndex
        int keyIndex = getKeyIndex(keyVer, enstr.substring(0, 2));
        if (0 <= keyIndex) {
            try {
                code = DESCoder.decrypt(enstr.substring(3), DESCoder.initKey(historyKey.get(keyVer).get(keyIndex)));
            } catch (Exception ex) {
                log.error("decrpty enstr(" + enstr + ") failure", ex);
            }
        }
        return code;
    }

    /**
     * <pre>
     * 取密钥下标
     *
     * </pre>
     *
     * @param keyVer      String 密钥版本号
     * @param keyIndexStr String 解码因子
     * @return int
     */
    final private int getKeyIndex(String keyVer, String keyIndexStr) {
        if (isCurrentVersion(keyVer)) {
            return Math.abs(keyIndexStr.hashCode()) % currentKeylist.size();
        } else if (historyKey.containsKey(keyVer)) {
            if (0 < historyKey.get(keyVer).size())
                return Math.abs(keyIndexStr.hashCode()) % historyKey.get(keyVer).size();
        }
        return -1;
    }

    /**
     * <pre>
     * 判断密钥版本是否为当前版本
     *
     * </pre>
     *
     * @param keyVer String 密钥版本
     * @return boolean
     */
    final private boolean isCurrentVersion(String keyVer) {
        return this.currendKeyVersion.equals(keyVer);
    }

    /**
     * <pre>
     * 解密钥
     *
     * </pre>
     *
     * @param enkey String 密钥密文
     * @return String
     */
    final private String decryptKey(String enkey) {
        String code = "";
        if (StringUtils.isEmpty(enkey)) {
            return code;
        }
        try {
            if (!"".equals(resourceKey)) {
                code = DESCoder.decrypt(enkey, DESCoder.initKey(resourceKey));
            } else {
                log.error("resourceKey is empty,to decrypt key failure,resourceKey=''");
            }
        } catch (Exception ex) {
            log.error(resourceKey + ",decrypt key(" + enkey + ") exception", ex);
        }
        return code;
    }

    public void setResourceKey(String resourceKey) {
        if (BASE64Coder.isBase64(resourceKey)) {
            this.resourceKey = new String(BASE64Coder.decodeBase(resourceKey));
        } else {
            log.error("resourceKey set failure,resourceKey=" + resourceKey);
            this.resourceKey = "";
        }
    }

    public void setCurrendKeyVersion(String currendKeyVersion) {
        this.currendKeyVersion = currendKeyVersion;
    }

    public void setCurrentKeylist(List<String> currentKeylist) {
        if (null != currentKeylist) {
            List<String> keylist = new ArrayList<String>();
            for (String enkey : currentKeylist) {
                if (BASE64Coder.isBase64(enkey)) {
                    String key = decryptKey(new String(BASE64Coder.decodeBase(enkey)));
                    if (!"".equals(key)) {
                        keylist.add(key);
                    } else {
                        log.error("decrypt enkey failure,enkey=" + enkey);
                        break;
                    }
                } else {
                    log.error("base64 decrypt enkey failure,enkey=" + enkey);
                    break;
                }
            }
            if (currentKeylist.size() == keylist.size()) {
                this.currentKeylist = keylist;
            } else {
                log.error("init currentKeylist failure,the size of decrypted keylist dosen't equal the size of param currentKeylist");
            }
        }
    }

    public void setHistoryKey(Map<String, List<String>> historyKey) {
        if (null != historyKey && !historyKey.isEmpty()) {
            Map<String, List<String>> hkeym = new HashMap<String, List<String>>();
            for (Map.Entry<String, List<String>> hkeyEntry : historyKey.entrySet()) {
                List<String> hkeylist = new ArrayList<String>();
                for (String enhkey : hkeyEntry.getValue()) {
                    String hkey = decryptKey(new String(BASE64Coder.decodeBase(enhkey)));
                    if (!"".equals(enhkey)) {
                        hkeylist.add(hkey);
                    } else {
                        log.error("decrypt enhkey failure,keyVer=" + hkeyEntry.getKey() + ",enhkey=" + enhkey);
                        break;
                    }
                }
                // 一组完整密钥全部初始成功方才初始
                if (hkeyEntry.getValue().size() != hkeylist.size()) {
                    break;
                }
                hkeym.put(hkeyEntry.getKey(), hkeylist);
            }
            if (historyKey.size() == hkeym.size()) {
                this.historyKey = hkeym;
            } else {
                log.error("init historyKey failure,the size of decrypted hkeym dosen't equal the size of param historyKey");
            }
        }
    }

}
