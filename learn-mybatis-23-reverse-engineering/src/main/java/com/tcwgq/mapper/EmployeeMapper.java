package com.tcwgq.mapper;

import com.tcwgq.po.Employee;

public interface EmployeeMapper {
    /**
     * 根据主键删除数据库的记录
     * @param id
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入数据库记录
     * @param record
     */
    int insert(Employee record);

    /**
     * @param record
     */
    int insertSelective(Employee record);

    /**
     * 根据主键获取一条数据库记录
     * @param id
     */
    Employee selectByPrimaryKey(Integer id);

    /**
     * @param record
     */
    int updateByPrimaryKeySelective(Employee record);

    /**
     * 根据主键来更新数据库记录
     * @param record
     */
    int updateByPrimaryKey(Employee record);
}