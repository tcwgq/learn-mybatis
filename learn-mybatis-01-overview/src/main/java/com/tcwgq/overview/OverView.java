package com.tcwgq.overview;
/**
 * @author tcwgq
 * @time 2017年9月6日下午10:02:35
 * @email tcwgq@outlook.com
 */
public class OverView {
/**
 * 课程安排：
mybatis和springmvc通过订单商品 案例驱动

第一天：基础知识（重点，内容量多）
	对原生态jdbc程序（单独使用jdbc开发）问题总结
	mybatis框架原理	（掌握）
	mybatis入门程序
		用户的增、删、改、查
	mybatis开发dao两种方法：
		原始dao开发方法（程序需要编写dao接口和dao实现类）（掌握）
		mybaits的mapper接口（相当于dao接口）代理开发方法（掌握）
	mybatis配置文件SqlMapConfig.xml
	mybatis核心：
		mybatis输入映射（掌握）
		mybatis输出映射（掌握）
	mybatis的动态sql（掌握）
	
第二天：高级知识
	订单商品数据模型分析
	高级结果集映射（一对一、一对多、多对多）
	mybatis延迟加载
	mybatis查询缓存（一级缓存、二级缓存）
	mybaits和spring进行整合（掌握）
	mybatis逆向工程
 */
}
