package com.tcwgq.mapper;

import java.util.List;

import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年9月13日下午9:58:05
 * @email tcwgq@outlook.com
 */
public interface UserMapper {

	public int insertUser(User user) throws Exception;

	public User getById(Integer id) throws Exception;

	public List<User> getByName(String name) throws Exception;

	public int updateUser(User user) throws Exception;

	public int deleteById(Integer id) throws Exception;
}
