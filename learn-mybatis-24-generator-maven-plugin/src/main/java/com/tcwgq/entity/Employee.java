package com.tcwgq.entity;

import java.util.Date;

public class Employee {
    /**
     * 主键
     * 表字段 : id
     */
    private Integer id;

    /**
     * 姓名
     * 表字段 : emp_name
     */
    private String empName;

    /**
     * 年龄
     * 表字段 : emp_age
     */
    private Integer empAge;

    /**
     * 创建人
     * 表字段 : creator
     */
    private String creator;

    /**
     * 创建时间
     * 表字段 : created
     */
    private Date created;

    /**
     * 修改人
     * 表字段 : modifier
     */
    private String modifier;

    /**
     * 修改时间
     * 表字段 : modified
     */
    private Date modified;

    /**
     * 
     * 表字段 : deleted
     */
    private Integer deleted;

    /**
     * 获取：主键
     * @return id：主键
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：主键
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：姓名
     * @return emp_name：姓名
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * 设置：姓名
     * @param empName
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    /**
     * 获取：年龄
     * @return emp_age：年龄
     */
    public Integer getEmpAge() {
        return empAge;
    }

    /**
     * 设置：年龄
     * @param empAge
     */
    public void setEmpAge(Integer empAge) {
        this.empAge = empAge;
    }

    /**
     * 获取：创建人
     * @return creator：创建人
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 设置：创建人
     * @param creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 获取：创建时间
     * @return created：创建时间
     */
    public Date getCreated() {
        return created;
    }

    /**
     * 设置：创建时间
     * @param created
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * 获取：修改人
     * @return modifier：修改人
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 设置：修改人
     * @param modifier
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    /**
     * 获取：修改时间
     * @return modified：修改时间
     */
    public Date getModified() {
        return modified;
    }

    /**
     * 设置：修改时间
     * @param modified
     */
    public void setModified(Date modified) {
        this.modified = modified;
    }

    /**
     * 获取：
     * @return deleted：
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 设置：
     * @param deleted
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}