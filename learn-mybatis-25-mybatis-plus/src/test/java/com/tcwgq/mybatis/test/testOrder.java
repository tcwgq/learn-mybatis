package com.tcwgq.mybatis.test;

import com.tcwgq.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试事务回滚机制
 * <p>
 * Created by tcwgq on 2018/10/9 22:45.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class testOrder {
    @Autowired
    private OrderService orderService;

    @Test
    public void testInsert1() {
        orderService.insert1();
    }

    @Test
    public void testInsert2() {
        orderService.insert2();
    }
}
