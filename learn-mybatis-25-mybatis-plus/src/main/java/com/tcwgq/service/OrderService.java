package com.tcwgq.service;

import com.tcwgq.entity.Order;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-02 14:36:55
 */
public interface OrderService extends IService<Order> {

    public int insert1();

    public int insert2();

}
