package com.tcwgq.service.impl;

import com.tcwgq.entity.Order;
import com.tcwgq.mapper.OrderMapper;
import com.tcwgq.service.OrderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-02 14:36:55
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Override
    // @Transactional
    public int insert1() {
        Order order = new Order();
        order.setInfo("aaa");
        order.setPrice(BigDecimal.valueOf(12.34));
        insert(order);
        insert2();
        return 0;
    }

    @Override
    @Transactional
    public int insert2() {
        Order order = new Order();
        order.setInfo("bbb");
        order.setPrice(BigDecimal.valueOf(45.67));
        insert(order);
        int a = 10 / 0;
        return 0;
    }
}
