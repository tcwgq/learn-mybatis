package com.tcwgq.enums;

public enum OrderStatus {
	FINISHED(1, "已完成"), PAYED(2, "已支付");

	private Integer status;
	private String description;

	private OrderStatus(Integer status, String description) {
		this.status = status;
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static OrderStatus getEnum(int status) {
		for (OrderStatus os : OrderStatus.values()) {
			if (os.getStatus() == status) {
				return os;
			}
		}
		return null;
	}

}
