package com.tcwgq.mybatis.handler;

import com.baomidou.mybatisplus.mapper.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Date;

public class MyMetaObjectHandler extends MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();
        Object timeCreated = getFieldValByName("timeCreated", metaObject);
        if (timeCreated == null) {
            setFieldValByName("timeCreated", date, metaObject);
        }
        Object timeModified = getFieldValByName("timeModified", metaObject);
        if (timeModified == null) {
            setFieldValByName("timeModified", date, metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Object timeModified = getFieldValByName("timeModified", metaObject);
        if (timeModified == null) {
            setFieldValByName("timeModified", new Date(), metaObject);
        }
    }

    /**
     * 开启插入填充
     */
    @Override
    public boolean openInsertFill() {
        return true;
    }

    /**
     * 开启更新填充
     */
    @Override
    public boolean openUpdateFill() {
        return true;
    }

}
