package com.tcwgq.generator;

import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.apache.commons.lang.time.DateFormatUtils;

import java.util.*;

/**
 * Created by tcwgq on 2018/10/2 0:49.
 */
public class MybatisGenerator {
    // 生成文件所在项目路径
    private static String baseProjectPath = System.getProperty("user.dir");
    // 基本包名
    private static String basePackage = "com.tcwgq";
    // 作者
    private static String authorName = "tcwgq";
    // 要生成的表名
    private static String[] tables = {"t_order"};
    // table前缀
    private static String prefix = "t_";
    // 数据库配置四要素
    private static String url = "jdbc:mysql://localhost:3306/learn?useUnicode=true&useSSL=false&characterEncoding=utf8";
    private static String driverName = "com.mysql.jdbc.Driver";
    private static String username = "root";
    private static String password = "112113";

    public static void main(String[] args) {
        System.out.println("============================== 开始 ============================");
        AutoGenerator gen = new AutoGenerator();
        /**
         * 数据库配置
         */
        gen.setDataSource(new DataSourceConfig()
                .setDbType(DbType.MYSQL)
                .setDriverName(driverName)
                .setUrl(url)
                .setUsername(username)
                .setPassword(password)
                .setTypeConvert(new MySqlTypeConvert() {
                    // 自定义数据库表字段类型转换【可选】
                    @Override
                    public DbColumnType processTypeConvert(String fieldType) {
                        // System.out.println("转换类型：" + fieldType);
                        // if ( fieldType.toLowerCase().contains( "tinyint" ) ) {
                        //    return DbColumnType.BOOLEAN;
                        // }
                        return super.processTypeConvert(fieldType);
                    }
                }));

        /**
         * 全局配置
         */
        gen.setGlobalConfig(new GlobalConfig()
                .setOutputDir(baseProjectPath + "/src/main/java")//输出目录
                .setFileOverride(true)// 是否覆盖文件
                .setActiveRecord(false)// 开启 activeRecord 模式
                .setEnableCache(false)// XML 二级缓存
                .setBaseResultMap(true)// XML ResultMap
                .setBaseColumnList(true)// XML columnList
                .setOpen(false)//生成后打开文件夹
                .setAuthor(authorName)
                // 自定义文件命名，注意 %s 会自动填充表实体属性！
                .setMapperName("%sMapper")
                .setXmlName("%sMapper")
                .setServiceName("%sService")
                .setServiceImplName("%sServiceImpl")
                .setControllerName("%sController")
        );

        /**
         * 策略配置
         */
        List<TableFill> tableFillList = new ArrayList<>();
        TableFill timeCreated = new TableFill("time_created", FieldFill.INSERT);
        TableFill timeModified = new TableFill("time_modified", FieldFill.INSERT_UPDATE);
        tableFillList.add(timeCreated);
        tableFillList.add(timeModified);
        gen.setStrategy(new StrategyConfig()
                // .setCapitalMode(true)// 全局大写命名
                // .setDbColumnUnderline(true)//全局下划线命名
                 .setTablePrefix(new String[]{prefix})// 此处可以修改为您的表前缀
                .setNaming(NamingStrategy.underline_to_camel)// 表名生成策略
                .setInclude(tables) // 需要生成的表
                // .setExclude(new String[]{"test"}) // 排除生成的表
                // 自定义实体父类
                 .setSuperEntityClass("com.tcwgq.entity.BaseEntity")
                // 自定义实体，公共字段，限制哪些字段在子类中不再产生，与表中字段一一对应
                 .setSuperEntityColumns(new String[]{"id", "version", "deleted", "time_created", "time_modified"})
                //.setVersionFieldName("version")
                // .setLogicDeleteFieldName("deleted")
                 .setTableFillList(tableFillList)
                // 自定义 mapper 父类 默认BaseMapper
                .setSuperMapperClass("com.baomidou.mybatisplus.mapper.BaseMapper")
                // 自定义 service 父类 默认IService
                // .setSuperServiceClass("com.baomidou.demo.IService")
                // 自定义 service 实现类父类 默认ServiceImpl
                // .setSuperServiceImplClass("com.baomidou.demo.ServiceImpl")
                // 自定义 controller 父类
                 .setSuperControllerClass("com.tcwgq.controller.BaseController")
                // 【实体】是否生成字段常量（默认 false）
                //  public static final String PRICE = "price"; public static final String INFO = "info";
                // .setEntityColumnConstant(true)
                // 【实体】是否为构建者模型（默认 false）
                // public Order setPrice(BigDecimal price) { this.price = price;return this; }
                // .setEntityBuilderModel(true)
                // 【实体】是否为lombok模型（默认 false）<a href="https://projectlombok.org/">document</a>
                // .setEntityLombokModel(true)
                //  Boolean类型字段是否移除is前缀处理
                // .setEntityBooleanColumnRemoveIsPrefix(true)
                // .setRestControllerStyle(true)
                // .setControllerMappingHyphenStyle(true)

        );

        /**
         * 包配置
         */
        gen.setPackageInfo(new PackageConfig()
                //.setModuleName("admin")
                .setParent(basePackage)// 自定义包路径
                .setEntity("entity")
                .setMapper("mapper")
                .setService("service")
                .setServiceImpl("service.impl")
                .setController("controller")// 这里是控制器包名，默认 web
                .setXml("mapper")
        );

        /**
         * 注入自定义配置
         */
        // 注入自定义配置，可以在 VM 中使用 cfg.abc 设置的值
        InjectionConfig abc = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<>();
                map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                map.put("time", DateFormatUtils.format(new Date(), "HH:mm:ss"));
                this.setMap(map);
            }
        };

        // 自定义文件输出位置（非必须），不配置，默认输出到项目指定目录
        PackageConfig packageInfo = gen.getPackageInfo();
        // 此处不使用 (packageInfo.getModuleName() == null ? "" : packageInfo.getModuleName())判断，会产生两个目录，一个moduleName为null的，另一个为moduleName为空的
        String path = baseProjectPath + "/src/main/java/com/tcwgq/" + (packageInfo.getModuleName() == null ? "" : packageInfo.getModuleName()) + "/";
        List<FileOutConfig> fileOutList = new ArrayList<>();
        fileOutList.add(new FileOutConfig("templates/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                //return "D:/Mappers/" + tableInfo.getEntityName() + "Mapper.xml";
                return baseProjectPath + "/src/main/resources/mappers/" + tableInfo.getEntityName() + "Mapper.xml";
            }
        });
        fileOutList.add(new FileOutConfig("templates/entity.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return path + packageInfo.getEntity() + "/" + tableInfo.getEntityName() + ".java";
            }
        });
        fileOutList.add(new FileOutConfig("templates/mapper.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return path + packageInfo.getMapper() + "/" + tableInfo.getEntityName() + "Mapper.java";
            }
        });
        fileOutList.add(new FileOutConfig("templates/service.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return path + packageInfo.getService() + "/" + tableInfo.getEntityName() + "Service.java";
            }
        });
        fileOutList.add(new FileOutConfig("templates/serviceImpl.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return path + "service/impl/" + tableInfo.getEntityName() + "ServiceImpl.java";
            }
        });
        fileOutList.add(new FileOutConfig("templates/controller.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return path + packageInfo.getController() + "/" + tableInfo.getEntityName() + "Controller.java";
            }
        });

        abc.setFileOutConfigList(fileOutList);
        gen.setCfg(abc);

        /**
         * 指定模板引擎 默认是VelocityTemplateEngine，需要引入相关引擎依赖
         */
//        gen.setTemplateEngine(new FreemarkerTemplateEngine());

        /**
         * 模板配置
         */
        //gen.setTemplate(
        // 关闭默认 xml 生成，调整生成 至 根目录
        // new TemplateConfig().setXml(null)
        // 自定义模板配置，模板可以参考源码 /mybatis-plus/src/main/resources/template 使用 copy
        // 至您项目 src/main/resources/template 目录下，模板名称也可自定义如下配置：
        // .setController("...");
        // .setEntity("...");
        // .setMapper("...");
        // .setXml("...");
        // .setService("...");
        // .setServiceImpl("...")
        //);

        // 执行生成
        gen.execute();
        System.out.println("============================== 结束 ============================");
    }
}
