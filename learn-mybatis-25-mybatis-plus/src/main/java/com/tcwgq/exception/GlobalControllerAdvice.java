package com.tcwgq.exception;

import com.tcwgq.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.IOException;

/**
 * 通用异常处理器，返回json格式的错误信息，不能跳转到指定页面
 * 404, 500等通用错误页面跳转，不建议使用ControllerAdvice实现，可直接在web.xml中配置404,500等错误页面
 * Created by tcwgq on 2018/10/2 14:14.
 */
//@ResponseBody
//@ControllerAdvice
public class GlobalControllerAdvice {
    @ExceptionHandler(BusinessRuntimeException.class)
    //@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiResponse<ErrorCode> business(BusinessRuntimeException e) {
        return ApiResponse.fail(e);
    }

    @ExceptionHandler(MyRuntimeException.class)
    //@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiResponse<ErrorCode> error(MyRuntimeException e) {
        return ApiResponse.fail(e);
    }

    @ExceptionHandler(value = {IOException.class, RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView exception(Exception exception, WebRequest request) {
        return new ModelAndView("error");
    }

    @ExceptionHandler(value = {NoHandlerFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView noMapping(Exception exception, WebRequest request) {
        return new ModelAndView("noFound");
    }

    //@ResponseBody
    //@ExceptionHandler(RuntimeException.class)
    //public ModelAndView error(RuntimeException e) {
    //    // 注意ModelAndView不能放在方法参数中进行参数绑定，否则直接在页面显示原始错误信息
    //    ModelAndView modelAndView = new ModelAndView();
    //    modelAndView.setViewName("error");
    //    modelAndView.addObject("msg", e.getMessage());
    //    return modelAndView;
    //}
}
