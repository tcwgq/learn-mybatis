package com.tcwgq.controller;


import com.tcwgq.entity.Order;
import com.tcwgq.exception.BusinessRuntimeException;
import com.tcwgq.exception.MyRuntimeException;
import com.tcwgq.mapper.OrderMapper;
import com.tcwgq.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-02 14:36:55
 */
@Controller
@RequestMapping("/order")
public class OrderController extends BaseController {
    @Autowired
    private OrderMapper mapper;

    @ResponseBody
    @RequestMapping(value = "/get.action", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResponse<Order> get(Integer id) {
        if (id == 0) {
            throw new BusinessRuntimeException(123, "BusinessRuntimeException");
        }
        if (id == 1) {
            throw new MyRuntimeException(456, "MyRuntimeException");
        }
        return ApiResponse.success(mapper.selectById(id));
    }

    @RequestMapping(value = "/index", method = {RequestMethod.GET, RequestMethod.POST})
    public String index(Integer id) {
        if (id == 0) {
            throw new BusinessRuntimeException(123, "BusinessRuntimeException");
        }
        if (id == 1) {
            throw new MyRuntimeException(456, "MyRuntimeException");
        }
        return "index";
    }
}

