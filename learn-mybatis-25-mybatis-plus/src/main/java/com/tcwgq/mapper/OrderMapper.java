package com.tcwgq.mapper;

import com.tcwgq.entity.Order;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author tcwgq
 * @since 2018-10-02 14:36:55
 */
public interface OrderMapper extends BaseMapper<Order> {

}
