package com.tcwgq.mybatis.test;

import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.tcwgq.mapper.UserMapper;
import com.tcwgq.model.User;
import com.tcwgq.model.UserCustom;
import com.tcwgq.model.UserQueryVo;

/**
 * @author tcwgq
 * @time 2017年9月15日下午10:05:50
 * @email tcwgq@outlook.com
 */
public class UserMapperTest {
	private SqlSessionFactory sqlSessionFactory;

	private UserMapper mapper;

	@Before
	public void setUp() throws Exception {
		// 不能加classpath前缀
		InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
		// Configuration config = new Configuration();
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);

		SqlSession session = sqlSessionFactory.openSession();

		mapper = session.getMapper(UserMapper.class);
	}

	@Test
	public void testInsertUser() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetById() throws Exception {
		User user = mapper.getById(10);
		System.out.println(user);
	}

	@Test
	public void testGetByName() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateUser() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetByCriteria() throws Exception {
		UserQueryVo vo = new UserQueryVo();
		UserCustom custom = new UserCustom();
		custom.setSex("1");
		custom.setUsername("张三丰");
		vo.setUserCustom(custom);
		List<User> list = mapper.getByCriteria(vo);
		System.out.println(list);
	}

	@Test
	public void testDeleteById() {
		fail("Not yet implemented");
	}

}
